﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(string email,string pwd)
        {

            Entities db = new Entities();
            var user = db.User.Where(d => d.Email == email && d.Password == pwd).FirstOrDefault();
            if (user != null)
            {
                return RedirectToAction("Index");
            }
            else {
                ViewBag.ccc = "帳號密碼錯誤，請重新輸入";
                return View();
            }
            
        }
	}
}